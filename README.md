# Mini-FALL3D

A FALL3D mini-app was developed for both CPU & GPU using OpenACC directives in the latter case. This mini-app only solves the FALL3D task assuming a fixed problem, i.e., helicoidal velocity field. This FALL3D task involves fully solving the main computational kernel, i.e., solves a set of advection-diffusion-sedimentation (ADS) equations on a structured grid using a second order finite volume explicit scheme (Euler or RK 4th in time). As stated, the ADS kernel is fully computed through GPU cards, i.e., 100% 

## Documentation
For further information, please visit the FALL3D 
[User Guide](https://fall3d-suite.gitlab.io/fall3d/).

## Developers
FALL3D is developed and maintained by the **Consejo Superior de
Investigaciones Cientificas (CSIC)** and the **Istituto Nazionale di
Geofisica e Vulcanologia (INGV)**

1. [Arnau Folch](@afolch) 
2. [Antonio Costa](@ant.costa)
3. [Giovanni Macedonio](@gmacedonio)
4. [Leonardo Mingari](@lmingari)

## License and copyright
Copyright (C) 2013, 2016, 2017, 2018, 2019, 2021  Arnau Folch, Leonardo Mingari, 
Antonio Costa and Giovanni Macedonio

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.

See the GNU General Public License 
[here](https://gitlab.com/fall3d-suite/fall3d/-/blob/master/LICENSE).